//Translate Data models into codes


//Users Collection

{
	"_id" : "user001",
	"firstName" : "John",
	"lastName" : "Smith",
	"email" : "johnsmith@mail.com",
	"password" : "johnsmith123",
	"isAdmin" : true,
	"mobileNumber" : "09123456789",
	"orders" : ["order002"],
	"dateTimeRegistered" : "2022-06-10T15:00:00.00Z"  //dateTime datatype 

}

{
		
	"_id" : "user002",
	"firstName" : "Jane",
	"lastName" : "Doe",
	"email" : "janedoe@mail.com",
	"password" : "johnsmith123",
	"isAdmin" : false,
	"mobileNumber" : "09123123123",
	"orders" : ["order001"], 
	"dateTimeRegistered" : "2022-06-10T15:00:00.00Z" 
}

//Product Collection (product.json)

{
	"_id" : "product001",
	"name" : "lenovo laptop",
	"description" : " Intel’s latest 11th Gen processors and Iris Xe integrated graphics deliver snappy performance with no slowdown or freezes that we observed. ",
	"price" : 51000,
	"isActive" : true,
	"stocks" : 19,
	"order" : ["user001"],
	"SKU" : "UGG-BB-PUR-01",
	"dateTimeCreated" : "2022-06-10T15:00:00.00Z" 
}

{
	"_id" : "product002",
	"name" : "CSS",
	"description" : "Zephyrus G15 is one of the lightest 15-inch gaming laptops you can buy.",
	"price" : 62000,
	"isActive" : true,
	"stocks" : 9,
	"order" : ["user002"],
	"SKU" : "UGG-BB-PUR-02",
	"dateTimeCreated" : "2022-06-10T15:00:00.00Z"  
}

//orders Collection (Order.json)

{
	"_id" : "order001",
	"userId" : "user002",
	"TransactionDate" : "2022-06-10T15:00:00.00Z" ,
	"status" : "schedule for delivery",
	"total" : "51000"  
}

{
	"_id" : "order002",
	"userId" : "user002",
	"TransactionDate" : "2022-06-10T15:00:00.00Z" ,
	"status" : "schedule for delivery",
	"total" : "51000" 
}

//Order Products Collection (OrderProduct.json)

{
	"_id" : "orderProduct001",
	"orderId" : "user002",
	"productId" : "user001",
	"quantity" : "1" ,
	"status" : "schedule for delivery",
	"price" : "51000", 
	"subtotal" : "51000",  
}

{
	"_id" : "orderProduct002",
	"userId" : "user001",
	"productId" : "user002",
	"quantity" : "1" ,
	"status" : "schedule for delivery",
	"price" : "62000", 
	"subtotal" : "62000", 
}